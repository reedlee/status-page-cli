require 'interactor'

class BackupDb
  include Interactor

  def call
    storagy = FileStorage.new()
    path = context.path_to_backup
    raise StandardError.new('Path to back up is empty') if path.nil?
    context.result = storagy.create_backup(path)
  end
end
