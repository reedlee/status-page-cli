require 'interactor'

class PullStatuses
  include Interactor

  def call
    services = [BitbucketService.new, CloudFlareService.new, GitHubService.new, RubyGemsService.new]
    web_parser = WebParser.new
    result = []
    services.each do |service|
      builder = Builder.new(service, web_parser)
      service = builder.build
      service.save
      result.push(service.to_h)
    end

    context.result = result
  end
end