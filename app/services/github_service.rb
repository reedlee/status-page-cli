require_relative 'service'

class GitHubService < Service
  def post_initialize
    @name = 'GitHub'
    @url = 'https://www.githubstatus.com/'
  end
end
