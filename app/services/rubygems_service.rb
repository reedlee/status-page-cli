require_relative 'service'

class RubyGemsService < Service
  def post_initialize
    @name = 'RubyGems'
    @url = 'https://status.rubygems.org/'
  end
end
