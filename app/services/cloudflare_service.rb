require_relative 'service'

class CloudFlareService < Service
  def post_initialize
    @name = 'CloudFlare'
    @url = 'https://www.cloudflarestatus.com/'
  end
end
