require_relative 'service'

class BitbucketService < Service
  def post_initialize
    @name = 'Bitbucket'
    @url = 'https://bitbucket.status.atlassian.com/'
  end
end
