class Service
  attr_reader :name, :url
  attr_accessor :status, :timestamp

  def initialize(storage=FileStorage.new())
    @storage = storage
    post_initialize
  end

  def post_initialize
  end

  def set_timestamp
    @timestamp = Time.now.utc.strftime('%m.%d.%Y %H:%M:%S')
  end

  def to_h
    {
        name: @name,
        url: @url,
        status: @status,
        timestamp: @timestamp
    }
  end

  def to_s
    to_h.to_s
  end

  def save
    @storage.save(to_h)
  end
end