class Builder
  def initialize(service, parser)
    @service = service
    @parser = parser
  end

  def build
    doc = @parser.get_document(@service.url)
    @service.status = @parser.get_status(doc)
    @service.set_timestamp
    @service
  end
end
