require 'nokogiri'
require 'net/https'
require 'net/http'

class WebParser
  def get_document(url)
    uri = URI.parse(url)
    req = Net::HTTP::Get.new(uri.to_s)
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') {|http|
      http.request(req).body
    }
  end

  def get_status(html)
    doc = Nokogiri::HTML(html)
    xpath_rule = "//div[contains(@class, 'page-status')]"
    string = ''
    doc.xpath(xpath_rule).each do |link|
      string = link.content
    end

    string.strip
  end
end
