require 'fileutils'

class FileStorage
  DEFAULT_DB_PATH = "#{Dir.pwd}/storage/db"
  TMP_DIR_PATH = "#{Dir.pwd}/tmp/backup"

  def initialize(path_file=DEFAULT_DB_PATH)
    @path_file = path_file
    @path_dir = path_file.split('/')[0..-2].join('/').strip
  end

  def save(object)
    File.open(@path_file, 'a') {|f|
      f.puts object.to_s
    }

    true
  end

  def all
    history = []

    File.open(@path_file, 'r') do |f|
      f.each_line do |line|
        history.push(parse_hash(line))
      end
    end

    history
  end

  def parse_hash(line)
    pairs = line.scan(/:(\w+)\=\>"([^\"]*)"/).map {|k, v| [k.to_sym, v.strip]}
    Hash[pairs]
  end

  def create_backup(path_where_backup_will_be)
    system("mkdir -p #{TMP_DIR_PATH}")
    version = "backup-#{Time.now.strftime("%Y-%m-%d-%H-%M-%S")}"
    tmp_backup = "#{TMP_DIR_PATH}/#{version}.tar.gz"

    FileUtils.cd(@path_dir) do |dir|
      system("tar -czvf #{tmp_backup} ./")
      system("mv #{tmp_backup} #{path_where_backup_will_be}")
    end

    path_where_backup_will_be
  end

  def restore_backup(path_to_backup_archive)
    FileUtils.cd(@path_dir) do |dir|
      system("tar -xvzf #{path_to_backup_archive}")
    end

    true
  end
end