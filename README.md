# status-page-cli

Simple CLI application to check the status of websites:

* https://status.bitbucket.org/
* https://www.cloudflarestatus.com/
* https://status.rubygems.org/
* https://status.github.com/messages

[More Requirements](https://gitlab.com/reedlee/status-page-cli/wikis/home)

# Installation process

To run application you need to [install ruby 2.4.1](https://www.ruby-lang.org/en/documentation/installation/)

You need to install requirements for [Nokogiri gem](https://nokogiri.org/tutorials/installing_nokogiri.html)

After you need to run command
```bash
bundle install
sudo chmod +x ./status_page.rb
```
# How to use
You need to run command
```bash
./status_page.rb help
```
