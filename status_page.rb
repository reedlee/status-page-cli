#!/usr/bin/env ruby
require_relative './init'
require 'thor'

class StatusPageCli < Thor

  desc "pull", "Get stream of the page statuses"
  def pull
    say('Start to pull statuses...')
    statuses = PullStatuses.call
    print_in_columns [:name, :url, :status, :timestamp]
    print_table statuses.result
  end

  desc "live", "Get stream of page statuses"
  def live
    say('Start to pull statuses...')
    begin
      loop do
        if Time.now.sec%2 == 0
          statuses = PullStatuses.call
          print_table statuses.result
        end
      end
    rescue Interrupt => e
      say('Bye')
    end
  end

  desc "history", "Read history of the statuses from storage"
  def history
    say('Reading history')
    history = GetHistory.call
    print_table history.result
  end

  desc "backup --path path_with_name", "Create backup archive"
  option :path, alias: '-p', required: true
  def backup
    say('Make backup')
    path = options['path']
    BackupDb.call(path_to_backup: path)
    say("Please open #{path}")
  end

  desc "restore --path path_with_name", "Restore database from backup archive"
  option :path, alias: '-p', required: true
  def restore
    say('It will be replace DB with data from backup')
    result = ask("Enter y/n: ")
    if result == 'y'
      say('Restore from backup')
      path = options['path']
      RestoreDb.call(path_to_backup: path)
      say('Backup was restored')
    else
      say('Bye')
    end

  end
end

StatusPageCli.start(ARGV)