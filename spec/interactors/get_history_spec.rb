require 'spec_helper'

describe GetHistory do
  let(:bitbucket) {
    {
        name: 'Bitbucket',
        url: 'https://bitbucket.status.atlassian.com/',
        status: 'All Systems Operational',
        timestamp: '01.06.2018 10:41:38'
    }
  }

  let(:cloudflare) {
    {
        name: 'CloudFlare',
        url: 'https://www.cloudflarestatus.com/',
        status: 'Minor Service Outage',
        timestamp: '01.06.2018 10:41:38'
    }
  }

  let(:github) {
    {
        name: 'GitHub',
        url: 'https://www.githubstatus.com/',
        status: 'All Systems Operational',
        timestamp: '01.06.2018 10:41:38'
    }
  }

  let(:rubygems) {
    {
        name: 'RubyGems',
        url: 'https://status.rubygems.org/',
        status: 'All Systems Operational',
        timestamp: '01.06.2018 10:41:38'
    }
  }

  let(:history) {
    [bitbucket, cloudflare, github, rubygems]
  }
  let(:path) {
    "#{Dir.pwd}/spec/fixtures/storage/db_several_rows"
  }

  it 'read history from file' do
    expect(FileStorage).to receive(:new).and_return(FileStorage.new(path))
    records = GetHistory.call

    expect(records.result).to eq(history)
  end
end