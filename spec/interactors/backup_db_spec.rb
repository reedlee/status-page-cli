require 'spec_helper'

describe BackupDb do
  context 'create backup' do
    it 'get path' do
      expect(FileUtils).to receive(:cd)

      path = 'some_path'
      statuses = BackupDb.call(path_to_backup: path)
      expect(statuses.result).to eql(path)
    end

    it 'miss path' do
      expect{BackupDb.call}.to raise_error('Path to back up is empty')
    end
  end
end
