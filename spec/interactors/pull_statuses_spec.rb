require 'spec_helper'

describe PullStatuses do
  context 'get status of all web services' do
    let(:bitbucket) {
      {
          name: 'Bitbucket',
          url: 'https://bitbucket.status.atlassian.com/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    let(:cloudflare) {
      {
          name: 'CloudFlare',
          url: 'https://www.cloudflarestatus.com/',
          status: 'Minor Service Outage',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    let(:github) {
      {
          name: 'GitHub',
          url: 'https://www.githubstatus.com/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    let(:rubygems) {
      {
          name: 'RubyGems',
          url: 'https://status.rubygems.org/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    it 'get object' do
      allow(Time).to receive(:now) {
        Time.new(2018, 1, 6, 16, 41, 38)
      }
      html1 = File.read('./spec/fixtures/bitbucket/ok_index.html')
      html2 = File.read('./spec/fixtures/cloudflare/minor_index.html')
      html3 = File.read('./spec/fixtures/github/ok_index.html')
      html4 = File.read('./spec/fixtures/rubygems/ok_index.html')
      allow(Net::HTTP).to receive(:start).and_return(html1, html2, html3, html4)
      allow_any_instance_of(FileStorage).to receive(:save).and_return(:true)

      statuses = PullStatuses.call
      expect(statuses.result).to eql([bitbucket, cloudflare, github, rubygems])
    end
  end
end
