require 'spec_helper'

describe RestoreDb do
  context 'create backup' do
    it 'get path' do
      expect(FileUtils).to receive(:cd)

      path = 'some_path'
      statuses = RestoreDb.call(path_to_backup: path)
      expect(statuses.result).to eql(true)
    end

    it 'miss path' do
      expect{RestoreDb.call}.to raise_error('Path to back up is empty')
    end
  end
end
