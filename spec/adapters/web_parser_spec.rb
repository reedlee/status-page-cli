require 'spec_helper'

describe WebParser do
  let(:parser) {WebParser.new}
  let(:bitbucket_html){File.read('./spec/fixtures/bitbucket/ok_index.html')}
  it 'get_document' do
    url = 'https://bitbucket.status.atlassian.com/'

    allow(Net::HTTP).to receive(:start) {bitbucket_html}
    body = parser.get_document(url)
    expect(body).to match(/Welcome to Atlassian Bitbucket/)
  end

  context 'bitbucket' do
    it 'parse everything is ok' do
      status = parser.get_status(bitbucket_html)
      expect(status).to eql('All Systems Operational')
    end
  end

  context 'cloudflare' do
    it 'parse everything is minor service outage' do
      html = File.read('./spec/fixtures/cloudflare/minor_index.html')
      status = parser.get_status(html)
      expect(status).to eql('Minor Service Outage')
    end
  end

  context 'github' do
    it 'parse everything is minor service outage' do
      html = File.read('./spec/fixtures/github/ok_index.html')
      status = parser.get_status(html)
      expect(status).to eql('All Systems Operational')
    end
  end

  context 'rubygems' do
    it 'parse everything is ok' do
      html = File.read('./spec/fixtures/rubygems/ok_index.html')
      status = parser.get_status(html)
      expect(status).to eql('All Systems Operational')
    end
  end
end