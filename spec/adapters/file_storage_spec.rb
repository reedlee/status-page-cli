require 'spec_helper'

describe FileStorage do
  let(:path){
    "#{Dir.pwd}/spec/fixtures/storage/db"
  }

  let(:storage) {
    FileStorage.new(path)
  }

  it 'should serialize and deserialize' do
    File.open(path, 'w'){|f| f.write ''}

    service = {
        name: 'Bitbucket',
        url: 'https://bitbucket.status.atlassian.com/',
        status: 'All Systems Operational',
        timestamp: '01.06.2018 10:41:38'
    }

    expect(storage.save(service)).to eql(true)
    expect(storage.all).to eql([service])
  end

  it 'should parse string' do
    string = '{:name=>"Bitbucket", :url=>"https://bitbucket.status.atlassian.com/", :status=>"Up", :timestamp=>"01.06.2018 10:41:38"}'
    service = {
        name: 'Bitbucket',
        url: 'https://bitbucket.status.atlassian.com/',
        status: 'Up',
        timestamp: '01.06.2018 10:41:38'
    }

    expect(storage.parse_hash(string)).to eql(service)
  end

  it 'should create backup' do
    back_up_path = "#{Dir.pwd}/spec/fixtures/storage_for_backup/backup/backup_name.tar.gz"
    path = "#{Dir.pwd}/spec/fixtures/storage_for_backup/backup/db"
    storage = FileStorage.new(path)

    storage.create_backup(back_up_path)

    expect(File.exist?(back_up_path)).to be_truthy

    system("rm #{back_up_path}") if File.exist?(back_up_path)
  end

  it 'should restore backup' do
    path_to_db = "#{Dir.pwd}/spec/fixtures/storage_for_backup/restore/db"
    storage = FileStorage.new(path_to_db)
    backup_fle_path = "#{Dir.pwd}/spec/fixtures/storage_for_backup/restore/backup_name_for_restore.tar.gz"
    storage.restore_backup(backup_fle_path)

    service = [{
        name: 'Bitbucket',
        url: 'https://bitbucket.status.atlassian.com/',
        status: 'Up',
        timestamp: '01.06.2018 10:41:38'
    }]

    expect(storage.all).to eql(service)
    system("rm #{path_to_db}") if File.exist?(path_to_db)
  end
end