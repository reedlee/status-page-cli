require 'spec_helper'

describe BitbucketService do
  let(:service){
    {
        name: 'Bitbucket',
        url: 'https://bitbucket.status.atlassian.com/',
        status: nil,
        timestamp: nil
    }
  }

  it 'store object' do
    storage = double()
    expect(storage).to receive(:save).with(service)
    bitbucket_service = BitbucketService.new(storage)
    bitbucket_service.save
  end
end
