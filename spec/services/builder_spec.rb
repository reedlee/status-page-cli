require 'spec_helper'

describe Builder do
  context 'Bitbucket' do
    let(:expected_service) {
      {
          name: 'Bitbucket',
          url: 'https://bitbucket.status.atlassian.com/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    it 'get object' do
      storage = double()
      allow(Time).to receive(:now) {
        Time.new(2018, 1, 6, 16, 41, 38)
      }
      html = File.read('./spec/fixtures/bitbucket/ok_index.html')
      allow(Net::HTTP).to receive(:start) {html}
      service = BitbucketService.new(storage)
      builder = Builder.new(service, WebParser.new)

      expect(builder.build.to_h).to eql(expected_service)
    end
  end

  context 'CloudFlare' do
    let(:expected_service) {
      {
          name: 'CloudFlare',
          url: 'https://www.cloudflarestatus.com/',
          status: 'Minor Service Outage',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    it 'get object' do
      storage = double()
      allow(Time).to receive(:now) {
        Time.new(2018, 1, 6, 16, 41, 38)
      }
      html = File.read('./spec/fixtures/cloudflare/minor_index.html')
      allow(Net::HTTP).to receive(:start) {html}
      service = CloudFlareService.new(storage)
      builder = Builder.new(service, WebParser.new)

      expect(builder.build.to_h).to eql(expected_service)
    end
  end
  context 'GitHub' do
    let(:expected_service) {
      {
          name: 'GitHub',
          url: 'https://www.githubstatus.com/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    it 'get object' do
      storage = double()
      allow(Time).to receive(:now) {
        Time.new(2018, 1, 6, 16, 41, 38)
      }
      html = File.read('./spec/fixtures/github/ok_index.html')
      allow(Net::HTTP).to receive(:start) {html}
      service = GitHubService.new(storage)
      builder = Builder.new(service, WebParser.new)

      expect(builder.build.to_h).to eql(expected_service)
    end
  end

  context 'RubyGems' do
    let(:expected_service) {
      {
          name: 'RubyGems',
          url: 'https://status.rubygems.org/',
          status: 'All Systems Operational',
          timestamp: '01.06.2018 10:41:38'
      }
    }

    it 'get object' do
      storage = double()
      allow(Time).to receive(:now) {
        Time.new(2018, 1, 6, 16, 41, 38)
      }
      html = File.read('./spec/fixtures/rubygems/ok_index.html')
      allow(Net::HTTP).to receive(:start) {html}
      service = RubyGemsService.new(storage)
      builder = Builder.new(service, WebParser.new)

      expect(builder.build.to_h).to eql(expected_service)
    end
  end
end
